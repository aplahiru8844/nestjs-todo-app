import { HttpException } from '@nestjs/common';

export class CustomLoggingException extends HttpException {
  constructor(message: string, statusCode: number) {
    super(message, statusCode);
  }
}
