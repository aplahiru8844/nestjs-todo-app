import { Inject, Injectable } from '@nestjs/common';
import { repositories } from 'src/constants';
import { createTodoDto } from 'src/dtos/createTodoDto';
import { Todo } from './todo.entity';

@Injectable()
export class TodoService {
  constructor(
    @Inject(repositories.TODO_REPOSITORY)
    private todoRepository: typeof Todo,
  ) {}

  async getTodoList(): Promise<Todo[]> {
    const todos = this.todoRepository.findAll<Todo>({
      where: { isDeleted: false },
    });
    return todos;
  }

  async getTodoById(id: string): Promise<Todo> {
    const todo = this.todoRepository.findByPk(id);
    return todo;
  }

  async createTodo(todo: createTodoDto): Promise<Todo> {
    const createdTodo = this.todoRepository.create({
      ...todo,
      isDeleted: false,
    });
    return createdTodo;
  }

  async updateTodo(
    todo: createTodoDto,
  ): Promise<[affectedCount: number, affectedRows: Todo[]]> {
    const updatedTodo = this.todoRepository.update(todo, {
      returning: true,
      where: { id: todo.id },
    });
    return updatedTodo;
  }

  async deleteTodo(
    id: number,
  ): Promise<[affectedCount: number, affectedRows: Todo[]]> {
    const deletedTodo = this.todoRepository.update(
      { isDeleted: true },
      { where: { id: id }, returning: true },
    );
    return deletedTodo;
  }
}
