import {
  Model,
  Column,
  Table,
  PrimaryKey,
  DataType,
  AutoIncrement,
} from 'sequelize-typescript';

@Table
export class Todo extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column({ type: DataType.INTEGER })
  id: number;
  @Column
  title: string;
  @Column
  description: string;
  @Column
  isDeleted: boolean;
}
