import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  ParseIntPipe,
  ParseUUIDPipe,
  Post,
  Put,
  UseFilters,
  UsePipes,
} from '@nestjs/common';
import { createTodoDto } from 'src/dtos/createTodoDto';
import { HttpExceptionFilter } from 'src/exceptions/exceptionFilter';
import { TodoService } from './todo.service';

@Controller('todos')
@UseFilters(HttpExceptionFilter)
export class TodoController {
  constructor(private todoService: TodoService) {}

  @Get()
  getTodoList() {
    try {
      const todos = this.todoService.getTodoList();
      return todos;
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.NOT_FOUND);
    }
  }

  @Get(':todoId')
  // @UsePipes(ParseUUIDPipe)
  // getMovieById(@Param('todoId', ParseUUIDPipe) todoId) {
  getTodoById(@Param('todoId') todoId) {
    try {
      const todo = this.todoService.getTodoById(todoId);
      return todo;
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.NOT_FOUND);
    }
  }

  @Post('create')
  createTodo(@Body() todo: createTodoDto) {
    try {
      const createdTodo = this.todoService.createTodo(todo);
      return createdTodo;
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.NOT_IMPLEMENTED);
    }
  }

  @Put('update')
  updateTodo(@Body() todo: createTodoDto) {
    try {
      const updatedTodo = this.todoService.updateTodo(todo);
      return updatedTodo;
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.NOT_IMPLEMENTED);
    }
  }

  @Delete('delete/:id')
  @UsePipes(ParseIntPipe)
  deleteTodo(@Param('id') id) {
    try {
      const deletedTodo = this.todoService.deleteTodo(id);
      return deletedTodo;
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.NOT_IMPLEMENTED);
    }
  }
}
