import { repositories } from 'src/constants';
import { Todo } from './todo.entity';

export const todoProviders = [
  {
    provide: repositories.TODO_REPOSITORY,
    useValue: Todo,
  },
];
