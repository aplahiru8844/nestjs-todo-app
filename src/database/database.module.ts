import { Module } from '@nestjs/common';
import { databaseProviders } from 'src/configs/database.config';

@Module({
  providers: [...databaseProviders],
  exports: [...databaseProviders],
})
export class DatabaseModule {}
