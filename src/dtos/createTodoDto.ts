export class createTodoDto {
  readonly id?: string;
  readonly title: string;
  readonly description: string;
}
